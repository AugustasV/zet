# Main Chaos Engineering tools

There are quite a few chaos engineering tools in the market nowadays. Some of them like AWS Fault Injection Simulator and AWS Resilience Hub, Azure Chaos Studio are created to be used only to their cloud environments, than others specified to be used with Kubernetes only, or virtual machines environments. 
* From open source tools Litmus is the most popular/supported one with many scenarios. 
* Chaos Mesh/Chaos blade could offer most of regular features, but same with Litmus it has pretty steap learning curve
* Istio could offer some chaos engineering experiments.
* Gremlin Chaos toolset looks like proper comercial offering, setup is pretty straight forward and intituative. 

Sources:
(Comparison of Chaos Engineer Tools)[https://www.gremlin.com/community/tutorials/chaos-engineering-tools-comparison/#which-tool-is-right-for-me]
