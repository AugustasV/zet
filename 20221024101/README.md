# TLD tests in terraform
* manual, end to end, integration, contact, unit tests from big to small
* test later development is: writing code, refactoring code, writing tests (not good, takes time)
* TDD (test driving development) fast  development time, learning curve, increase product development
* `terraform validate` could be used to avoid that plan pass but it fails in tf apply stage. Terraform variable validations, fmt command to fix formatting.
* there are also preconditions and postconditions testing
* testing tools is terratest and kitchen-terraform

Sources:
https://github.com/gruntwork-io/terratest
https://newcontext-oss.github.io/kitchen-terraform/
https://github.com/sebastianczech/iac-tld-devops
